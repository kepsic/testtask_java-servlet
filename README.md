# README #

This is a practical exercise for Fortumo java developers. It is a very simple application that has been implemented as a Java servlet.

The purpose of this servlet is to receive parallel requests from the end users, send them to processing in the backend, wait for the results to come in and return the results to the end user.

Parts that require implementation are in the io.fortumo.tasks.Process class.

### How do I get set up? ###
First clone the code

    ~$ git clone git@bitbucket.org:fortumo/testtask_java-servlet.git

Then do a testrun (please make sure that nothing is running on your 8080 port).
*Maven*

    ~/testtask_java-servlet$ mvn jetty:run

*Gradle*

    ~/testtask_java-servlet$ gradle jettyRun

Go and check if the localhost in port 8080 is responding (http://localhost:8080/). If it is then it's ctrl+c and kill the app. Go implement the requirements. It does run out of the box also on Netbeans and probably on other Java IDEs.


### Who do I talk to? ###
You probably know to whom to talk to, if you already have ended up with the link to this repo ;).


###The Solution###

General idea is put backend processed data to ttl cache.  In most cases processed data is available instantly.
Some cases backend processing times out and error with details send back to client. But this is not problem.
Processed data will be stored to cache  until its expires.
Optionally it's possible access proceeded data with uuid parameter. For example http://localhost:8080/Process?uuid=04a843d8-70a0-42c6-afb8-e1ab03980578

*Output*
All output is JSON encoded

