/* 
 * File: ICallbackListener.java
 * Created: 18:20:07 12-May-2015
 * Author: Fortumo OÜ
 * Copyright (C) Fortumo OĆ�, 2015
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package io.fortumo.backend;

import java.util.UUID;

/**
 *
 * @author marek
 */
public interface ICallbackListener {
    /**
     * Callback method that gets called when the backend has finished
     * processing it's data. The callback provides also the correlation id
     * that is provided to the 
     * @param id correlation ID that was provided with dispatch.
     * @param data response data from the callback system.
     */
    void callback(UUID id, Object data);
}
