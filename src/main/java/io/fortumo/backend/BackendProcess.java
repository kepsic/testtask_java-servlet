/*
 * File: BackendProcess.java
 * Created: 18:24:53 12-May-2015
 * Author: Marek Laasik
 * Copyright (C) Fortumo OÜ, 2015
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential
 */
package io.fortumo.backend;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

/**
 *
 * @author Marek Laasik <marek@fortumo.com>
 */
public class BackendProcess {

    /**
     * Singleton instance.
     */
    private static final BackendProcess instance = new BackendProcess();

    /**
     * List of the callback listener instances.
     */
    private ICallbackListener listener;

    private final Timer timer = new Timer();

    /**
     * Private constructor, no instantiations.
     */
    private BackendProcess() {
    }

    /**
     * Getter for the backend process singleton.
     *
     * @return instance of the <see cref="BackendProcess/> singleton.
     */
    public static final BackendProcess getInstance() {
        return instance;
    }

    /**
     * Registers callback listener to the BackendProcess instance.
     *
     * @param listener listener to be registered for the backend process.
     */
    public synchronized void registerCallbackListener(final ICallbackListener listener) {
        if (listener == null) {
            throw new IllegalArgumentException("ICallback listener cannot be null");
        }

        this.listener = listener;
    }

    /**
     * Dispatch an object to the backend process for processing. When calling
     * this method provide the uuid as a unique identifier coming from the
     * calling thread.
     *
     * @param uuid {@link UUID} unique identifier set by the calling thread.
     * @param data data item for processing in the backend.
     */
    public void dispatch(final UUID uuid, final Object data) {
        long delay = (long) (Math.random() * 3000L);
        this.timer.schedule(new TimerTask() {
            @Override
            public void run() {
                executeCallback(uuid, data);
            }
        }, delay);
    }

    /**
     * Executes callback listener synchronously.
     *
     * @param uuid {@link UUID} set by the calling thread.
     * @param data data item returned.
     */
    private synchronized void executeCallback(final UUID uuid, final Object data) {
        if (this.listener == null) {
            return;
        }
        this.listener.callback(uuid, data);
    }
}
