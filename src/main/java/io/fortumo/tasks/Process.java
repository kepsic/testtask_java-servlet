/*
 * File: Process.java
 * Created: 23:01:58 12-May-2015
 * Author: Marek Laasik <marek@fortumo.com>
 * Copyright (C) Fortumo OÜ, 2015
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential
 */
package io.fortumo.tasks;

import com.crunchify.tutorials.CrunchifyInMemoryCache;
import io.fortumo.backend.BackendProcess;
import io.fortumo.backend.ICallbackListener;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.UUID;
import java.util.concurrent.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 *
 * @author Marek Laasik <marek@fortumo.com>
 * @author Andres Kepler <andres@kepler.ee>
 */
@WebServlet(name = "Process", urlPatterns = {"/Process"})
public class Process extends HttpServlet implements ICallbackListener {
    // timeToLiveInSeconds = 2 seconds
    // timerIntervalInSeconds = 1 seconds
    // maxItems = 10000
    // Example form http://crunchify.com/how-to-create-a-simple-in-memory-cache-in-java-lightweight-cache/
    CrunchifyInMemoryCache<UUID, Object> cache = new CrunchifyInMemoryCache<UUID, Object>(2, 1, 10000);
    /**
     * JSON encoder
     *
     * @param status json encoded servlet response
     * @param uuid json encoded servlet response
     * @param errors json encoded servlet response
     * @throws IOException if an I/O error occurs
     */
    private JSONObject jsonStatus(String status,UUID uuid, String errors) throws IOException {
        JSONObject json = new JSONObject();
        json.put("status",status);
        json.put("uuid", String.format("%s", uuid));


        switch (status) {
            case "OK":
                        try {

                if (cache.get(uuid)!=null) {
                    log(String.format("key: %s , data: %s", uuid,cache.get(uuid)));
                    json.put("data", cache.get(uuid));
                } else {
                    log(String.format("key: %s data: empty", uuid));
                    json.put("data", String.format("%s", "null"));
                }
                } catch (NullPointerException e) {
                            log(String.format("key: %s data: empty", uuid));
                            json.put("data", String.format("%s", "null"));
                            e.printStackTrace();
                } finally {
                    cache.remove(uuid);
                }

                json.put("message", errors);
                break;
            case "error":
                json.put("errors", errors);
                break;
            default:
                json.put("message", errors);
                break;

        }


        return json;
    }



    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        final UUID uuid = UUID.randomUUID();
        final Object data = request;


        try (PrintWriter out = response.getWriter()) {

                String puid = request.getParameter("uuid");
                if (puid!=null) { // if uuid parameter exist
                        if (cache.isKey(UUID.fromString(puid))) { // does key exist in cache
                            handleParam(request.getParameter("uuid"), out); // Handle parameter
                        } else { // if param exist but not in cache
                            out.println(jsonStatus("error", UUID.fromString(puid), "uuid not found in queue")); // Handle
                        }
                } else { // If no params specified
                    handleProcess(response, uuid, data, out);
                }


            }

        }

    /**
     * Parameter handler
     *
     * @param uuid uuid to identify req
     * @param out servlet output
     * @throws IOException if an I/O error occurs
     */

    private void handleParam(String uuid, PrintWriter out) throws IOException {
        UUID uid = UUID.fromString(uuid); // UUID from String
        JSONObject jsontxt = jsonStatus("OK", uid, "DELAY"); //Encode json
        out.println(jsontxt); // send JSON encoded data to client
        log(String.format("Param out: %s", jsontxt));
    }

    /**
     * Main process handler
     *
     * @param response json encoede servlet response
     * @param uuid key for data handling
     * @param data main data
     * @param out serverlt client output
     * @throws IOException if an I/O error occurs
     */

    private void handleProcess(HttpServletResponse response, final UUID uuid, final Object data, PrintWriter out) throws IOException {
        //timeout example from http://stackoverflow.com/questions/17233038/how-to-implement-synchronous-method-timeouts-in-java
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<String> future = executor.submit(new Callable() {


            public String call() throws Exception {
                backend.dispatch(uuid, data); //Send data to backend
                return "OK";
            }
        });
        try {

            //If backend not responding throw exception
            String dispatch = future.get(2, TimeUnit.MILLISECONDS); //timeout is in 2 milliseconds
            JSONObject jsontxt = jsonStatus(dispatch, uuid, "NO-ERROR"); //encode json
            out.println(jsontxt); // send json encoded data to client
            log(String.format("Dispatch OK: %s", jsontxt));
        } catch (TimeoutException e) {
            response.setStatus(HttpServletResponse.SC_REQUEST_TIMEOUT); // Set to 408
            JSONObject jsontxt = jsonStatus("error", uuid, "timeout"); // encode json
            out.println(jsontxt); // send json encoded data to client
            log(String.format("Timeout: %s", jsontxt));
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        executor.shutdownNow();
    }


    @Override
    public void callback(UUID id, Object data) {
        cache.put(id,data); // put processed data to cache
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods.">
    /**
     * Initialise the servlet code.
     * @throws ServletException in case the initialisation fails.
     */
    @Override
    public void init() throws ServletException {
        super.init();
        this.backend.registerCallbackListener(this);
    }
    
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Private variables.">
    private final BackendProcess backend = BackendProcess.getInstance();
    // </editor-fold>
}
